import { useEffect } from 'react';
import authService from '../services/authService';

export default function Logout(props) {
    useEffect(() => {
        authService.logout();
        window.location = '/';
    }, []);
    return null;
}
