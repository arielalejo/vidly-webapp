import React from 'react';

const TableHeader = ({ onSort, columns, sortColumn: inputsortColumn }) => {
    const raiseSort = (path) => {
        const sortColumn = { ...inputsortColumn };

        if (path === sortColumn.path)
            sortColumn.order = sortColumn.order === 'asc' ? 'desc' : 'asc';
        else {
            sortColumn.path = path;
            sortColumn.order = 'asc';
        }

        onSort(sortColumn);
    };

    const setSortIcon = (column) => {
        if (column.path !== inputsortColumn.path) return null;
        if (inputsortColumn.order === 'asc')
            return <i className="fa fa-sort-asc" aria-hidden="true"></i>;
        return <i className="fa fa-sort-desc" aria-hidden="true"></i>;
    };

    return (
        <thead>
            <tr>
                {columns.map((column) => (
                    <th
                        key={column.label || column.key}
                        className="clickable"
                        onClick={() => {
                            if (column.path) raiseSort(column.path);
                        }}
                    >
                        {column.label} {setSortIcon(column)}
                    </th>
                ))}
            </tr>
        </thead>
    );
};

export default TableHeader;
