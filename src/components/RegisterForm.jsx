import Joi from 'joi-browser';
import React from 'react';
import { toast } from 'react-toastify';
import Form from './common/Form';
import * as userService from '../services/userService';
import authService from '../services/authService';

class RegisterForm extends Form {
    state = {
        data: {
            email: '',
            password: '',
            name: '',
        },
        errors: {},
    };

    schema = {
        email: Joi.string().email().required(),
        password: Joi.string().min(6).required(),
        name: Joi.string().required(),
    };

    doSubmit = async () => {
        try {
            const { headers } = await userService.registerUser(this.state.data);
            authService.saveRegisteredUser(headers);

            window.location = '/';
        } catch (error) {
            const { response } = error;
            if (response && response.status === 409) toast.error(response.data);
            if (response && response.status === 400) {
                toast.error(response.data);
                // const errors = { ...this.state.errors };
                // errors.username  = response.data; /* only for usename? */
                // this.setState({ errors });
            }
        }
    };

    render() {
        return (
            <div>
                <h1>Register</h1>
                <form onSubmit={this.handleSubmit}>
                    {this.renderInput('Email', 'email', 'email')}
                    {this.renderInput('Password', 'password', 'password')}
                    {this.renderInput('Username', 'name', 'text')}
                    {this.renderButton('Register!')}
                </form>
            </div>
        );
    }
}

export default RegisterForm;
