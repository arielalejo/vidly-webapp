import http from './httpService';
import jwtDecode from 'jwt-decode';

const token = 'token';

http.setHeaderJwt(getJwt());

export async function login(email, password) {
    const { headers } = await http.post('/auth', {
        email,
        password,
    });
    localStorage.setItem(token, headers['x-auth-token']);
}

export function getLoggedUser() {
    try {
        const jwt = localStorage.getItem(token);
        return jwtDecode(jwt);
    } catch (error) {
        return null;
    }
}

export function saveRegisteredUser(headers) {
    localStorage.setItem(token, headers['x-auth-token']);
}

export function logout() {
    localStorage.removeItem(token);
}

export function getJwt() {
    return localStorage.getItem(token);
}

export default {
    login,
    getLoggedUser,
    logout,
    saveRegisteredUser,
    getJwt,
};
