import React, { Component } from 'react';
import Counter from './Counter';

class Counters extends Component {
    render() {
        const {
            counters,
            onReset,
            onIncrement,
            onDelete,
            onDecrement,
        } = this.props;

        return (
            <div>
                <button onClick={onReset} className="btn btn-sm btn-info">
                    Reset
                </button>

                {counters.map((counter) => {
                    return (
                        <Counter
                            key={counter.id}
                            onDelete={onDelete}
                            onIncrement={onIncrement}
                            onDecrement={onDecrement}
                            counter={counter}
                        />
                    );
                })}
            </div>
        );
    }
}

export default Counters;
