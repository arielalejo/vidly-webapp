import React, { Component, Fragment } from 'react';
import Counters from './counter/Counters';
import SimpleNavBar from './counter/SimpleNavBar';

class App extends Component {
    state = {
        counters: [
            { id: 1, value: 3 },
            { id: 2, value: 0 },
            { id: 3, value: 2 },
            { id: 4, value: 1 },
        ],
    };

    handleReset() {
        const counters = this.state.counters.map((c) => {
            return { ...c, value: 0 };
        });

        this.setState({
            counters,
        });
    }

    handleIncrement(counter) {
        const counters = [...this.state.counters];

        /* ----------------------------------------------------------------- */
        /* 'counters' is a copy of this.state.counters, so if we modify 'counters'
           we are modifying DIRECTLY the state of this component -> DON'T ! 

           we need to clone the object at the given location, so we have a 
           different element than the one in the state  */
        /* ----------------------------------------------------------------- */
        const index = counters.indexOf(counter);
        counters[index] = { ...counter };
        counters[index].value++;

        this.setState({
            counters,
        });
    }

    handleDelete(counter) {
        const counters = this.state.counters.filter((c) => c.id !== counter.id);

        this.setState({
            counters,
        });
    }

    handleDecrement(counter) {
        const counters = [...this.state.counters];

        const index = counters.indexOf(counter);
        counters[index] = { ...counter };
        counters[index].value--;

        this.setState({
            counters,
        });
    }

    render() {
        return (
            <Fragment>
                <SimpleNavBar
                    totalCounters={
                        this.state.counters.filter((c) => c.value > 0).length
                    }
                />
                <main className="container">
                    <Counters
                        onDelete={this.handleDelete.bind(this)}
                        onIncrement={this.handleIncrement.bind(this)}
                        onDecrement={this.handleDecrement.bind(this)}
                        onReset={this.handleReset.bind(this)}
                        counters={this.state.counters}
                    />
                </main>
            </Fragment>
        );
    }
}

export default App;
