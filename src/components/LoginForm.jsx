import React from 'react';
import Joi from 'joi-browser';
import { toast } from 'react-toastify';
import Form from './common/Form';
import authService from '../services/authService';
import { Redirect } from 'react-router-dom';

class LoginForm extends Form {
    state = {
        data: {
            email: '',
            password: '',
        },
        errors: {},
    };

    schema = {
        email: Joi.string().email().required().label('Email'),
        password: Joi.string().required().label('Password'),
    };

    render() {
        if (authService.getLoggedUser()) return <Redirect to="/" />;
        return (
            <form onSubmit={this.handleSubmit}>
                {this.renderInput('Email', 'email')}
                {this.renderInput('Password', 'password', 'password')}
                {this.renderButton('Login')}
            </form>
        );
    }

    doSubmit = async () => {
        try {
            const { email, password } = this.state.data;
            await authService.login(email, password);

            const { state } = this.props.location;
            window.location = state ? state.from.pathname : '/';
        } catch (error) {
            const { response } = error;
            if (response && response.status === 400) toast.error(response.data);
        }
    };
}

export default LoginForm;
