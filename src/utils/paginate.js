export function paginate(array, startPage, pageLength) {
    const startIndex = (startPage - 1) * pageLength;
    return array.slice(startIndex, startIndex + pageLength);
}
