import React from 'react';
import Joi from 'joi-browser';
import Form from './common/Form';
import * as movieService from '../services/movieService';
import * as genreService from '../services/genreService';
import { toast } from 'react-toastify';

class MovieForm extends Form {
    state = {
        data: {
            _id: '',
            title: '',
            genreId: '',
            numberInStock: '',
            dailyRentalRate: '',
        },
        genres: [],
        errors: {},
    };

    schema = {
        _id: Joi.string().optional().allow(''),
        title: Joi.string().required().label('Title'),
        genreId: Joi.string().required().label('Genre'),
        numberInStock: Joi.number().integer().required().label('Stock'),
        dailyRentalRate: Joi.number().required().label('Daily Rate'),
    };

    componentDidMount() {
        this.populateGenres();
        this.populateMovie();
    }

    populateGenres = async () => {
        const { data: genres } = await genreService.getGenres();
        this.setState({ genres });
    };

    populateMovie = async () => {
        const { id } = this.props.match.params;
        if (id === 'new') return;

        try {
            const { data: movie } = await movieService.getMovie(id);

            this.setState({
                data: {
                    _id: movie._id,
                    title: movie.title,
                    genreId: movie.genre._id /* we only need the id */,
                    numberInStock: movie.numberInStock,
                    dailyRentalRate: movie.dailyRentalRate,
                },
            });
        } catch (error) {
            const { response } = error;
            if (
                response &&
                (response.status === 404 || response.status === 400)
            )
                this.props.history.replace('/not-found');
        }
    };

    doSubmit = async () => {
        try {
            await movieService.saveMovie(this.state.data);
            this.props.history.replace('/movies');
        } catch (error) {
            const { response } = error;
            if (response && response.status === 400)
                toast.error('Bad data in Form');
        }
    };

    render() {
        //if (!this.state.data._id) return null;

        return (
            <div>
                <h2>Movie </h2>
                <form onSubmit={this.handleSubmit}>
                    {this.renderInput('Title', 'title')}
                    {this.renderDropDown('Genre', 'genreId', this.state.genres)}
                    {this.renderInput('Number in Stock', 'numberInStock')}
                    {this.renderInput('Rate', 'dailyRentalRate')}
                    {this.renderButton('Save')}
                </form>
            </div>
        );
    }
}

export default MovieForm;
