import http from '../services/httpService';

export function getMovies() {
    return http.get(`/movies`);
}

export function getMovie(movieId) {
    return http.get(`/movies/${movieId}`);
}

export function saveMovie(movie) {
    const body = { ...movie };

    const id = body._id;
    delete body._id;

    if (id) {
        return http.put(`/movies/${id}`, body);
    } else {
        return http.post(`/movies`, body);
    }
}

export function deleteMovie(movieId) {
    return http.delete(`/movies/${movieId}`);
}
