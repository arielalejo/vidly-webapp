/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react';
import PropTypes from 'prop-types';

const Pagination = ({ pageLength, totalItems, currentPage, onPageClick }) => {
    const pages = Math.ceil(totalItems / pageLength);
    if (pages === 1) return null;

    const pagesArray = [];
    for (let i = 1; i < pages + 1; i++) {
        pagesArray.push(i);
    }

    return (
        <nav aria-label="Page navigation example">
            <ul style={{ cursor: 'pointer' }} className="pagination">
                {pagesArray.map((page) => (
                    <li
                        key={page}
                        className={
                            page === currentPage
                                ? 'page-item active'
                                : 'page-item'
                        }
                        onClick={() => onPageClick(page)}
                    >
                        <a className="page-link">{page}</a>
                    </li>
                ))}
            </ul>
        </nav>
    );
};

Pagination.propTypes = {
    pageLength: PropTypes.number.isRequired,
    totalItems: PropTypes.number.isRequired,
    currentPage: PropTypes.number.isRequired,
    onPageClick: PropTypes.func.isRequired,
};
export default Pagination;
