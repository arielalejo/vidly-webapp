import axios from 'axios';
import { toast } from 'react-toastify';
import { log } from './logger';

axios.defaults.baseURL = process.env.REACT_APP_API_URL;

axios.interceptors.response.use(null, (error) => {
    const isExpectedErr =
        error.response &&
        400 &&
        error.response.status >= 400 &&
        error.response.status < 500;

    if (!isExpectedErr) {
        log(error);
        toast.error('an unexpected error ocurred');
    }

    return Promise.reject(error);
});

const setHeaderJwt = (jwtToken) => {
    axios.defaults.headers.common['x-auth-token'] = jwtToken;
};

export default {
    get: axios.get,
    post: axios.post,
    patch: axios.patch,
    put: axios.put,
    delete: axios.delete,
    setHeaderJwt,
};
