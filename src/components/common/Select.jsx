import React from 'react';

const Select = (props) => {
    const { label, name, items, error, ...args } = props;

    return (
        <div className="form-group">
            <label htmlFor={name}>{label}</label>
            <select className="form-control" name={name} id={name} {...args}>
                <option value="">Select and option</option>
                {items.map((item) => (
                    <option value={item._id} key={item._id}>
                        {item.name}
                    </option>
                ))}
            </select>
            {error && <div className="alert alert-danger">{error}</div>}
        </div>
    );
};

export default Select;
