import React, { Component } from 'react';
import './Counter.css';

class Counter extends Component {
    render() {
        const { counter, onIncrement, onDelete, onDecrement } = this.props;
        let classes = this.setBadgeClassName();

        return (
            <div className="counter-container">
                <div className="label-item">
                    <span className={classes}>{this.getCount()}</span>
                </div>
                <div className="buttons-item">
                    <button
                        onClick={() => onIncrement(counter)}
                        className="btn badge-secondary btn-sm"
                    >
                        +
                    </button>
                    <button
                        onClick={() => onDecrement(counter)}
                        className="btn badge-secondary btn-sm"
                        disabled={counter.value === 0 ? 'disable' : ''}
                    >
                        -
                    </button>
                    <button
                        onClick={() => onDelete(counter)}
                        className="btn btn-sm btn-danger"
                    >
                        x
                    </button>
                </div>
            </div>
        );
    }

    setBadgeClassName() {
        const { value } = this.props.counter;

        let classes = 'badge m-2 p-2 badge-';
        classes += value === 0 ? 'warning' : 'primary';

        return classes;
    }

    getCount() {
        const { value } = this.props.counter;
        return value === 0 ? 'Zero' : value;
    }
}

export default Counter;
