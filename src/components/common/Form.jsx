import React, { Component } from 'react';
import Joi from 'joi-browser';
import Input from './Input';
import Select from './Select';

class Form extends Component {
    state = { data: {}, errors: {} };

    renderButton = (label) => {
        return (
            <button disabled={this.validate()} className="btn btn-secondary">
                {label}
            </button>
        );
    };

    renderInput = (label, fieldName, type = 'text') => {
        const { data: movie, errors } = this.state;

        return (
            <Input
                label={label}
                name={fieldName}
                error={errors[fieldName]}
                type={type}
                value={movie[fieldName]}
                onChange={this.handleChange}
            />
        );
    };

    renderDropDown = (label, fieldName, items) => {
        const { data: movie, errors } = this.state;

        return (
            <Select
                label={label}
                name={fieldName}
                items={items}
                error={errors[fieldName]}
                value={movie[fieldName]}
                onChange={this.handleChange}
            />
        );
    };

    handleSubmit = (e) => {
        e.preventDefault();

        /* validation */
        const validationErrors = this.validate();
        this.setState({
            ...this.state,
            errors: validationErrors || {},
        });

        /* if no errors request to a server... */
        if (!validationErrors) this.doSubmit();
    };

    handleChange = ({ target: input }) => {
        const data = { ...this.state.data };
        const errors = { ...this.state.errors };

        const errorMessage = this.validateProperty(input);

        /* if there is an error, fill the specific property in the errors object */
        /* if there's No error, then remove the given property in the errors object */
        if (errorMessage) errors[input.name] = errorMessage;
        else delete errors[input.name];

        /* update state */
        data[input.name] = input.value;
        this.setState({
            data,
            errors,
        });
    };

    validate = () => {
        const { data } = this.state;

        const options = { abortEarly: false };
        const { error } = Joi.object(this.schema).validate(data, options);
        if (!error) return null;

        const errors = {};
        error.details.forEach((err) => {
            errors[err.path[0]] = err.message;
        });

        return errors;
    };

    validateProperty = ({ name, value }) => {
        const obj = { [name]: value };
        const schema = { [name]: this.schema[name] };
        const { error } = Joi.object(schema).validate(obj);
        if (!error) return null;
        return error.details[0].message;
    };
}

export default Form;
