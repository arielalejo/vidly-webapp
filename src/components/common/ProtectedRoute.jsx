import React from 'react';
import { Redirect, Route } from 'react-router-dom';
import authService from '../../services/authService';

export default function ProtectedRoute(props) {
    const { path, component: Component, forRender, ...args } = props;
    const user = authService.getLoggedUser();

    return (
        <Route
            {...args}
            path={path}
            render={(props) => {
                /* props -> { history, location, match }*/
                if (user)
                    return Component ? <Component {...props} /> : forRender(props);
                else
                    return (
                        <Redirect
                            to={{
                                pathname: '/login',
                                state: { from: props.location },
                            }}
                        />
                    );
            }}
        />
    );
}
