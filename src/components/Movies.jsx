import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import _ from 'lodash';
import * as genreService from '../services/genreService';
import * as movieService from '../services/movieService';
import { paginate } from '../utils/paginate';
import Pagination from './common/Pagination';
import GroupList from './common/GroupList';
import MoviesTable from './MoviesTable';
import SearchBar from './SearchBar';
import { toast } from 'react-toastify';

class Movies extends Component {
    state = {
        movies: [],
        genres: [],
        pageLength: 3,
        currentPage: 1,
        sortColumn: { path: 'title', order: 'asc' },
        searchText: '',
    };

    componentDidMount() {
        this.doGetDataFromServer();
    }

    render() {
        const {
            currentPage,
            pageLength,
            selectedGenre,
            sortColumn,
            genres,
            searchText,
            movies: allMovies,
        } = this.state;

        let movies;
        let moviesCount;

        if (!searchText) {
            const result = this.prepareData();
            movies = result.movies;
            moviesCount = result.moviesCount;
        } else {
            movies = allMovies.filter((movie) =>
                movie.title.toLowerCase().includes(searchText.toLowerCase())
            );
            moviesCount = movies.length;
        }

        return (
            <div className="row">
                <div className="col-3">
                    <GroupList
                        items={genres}
                        selectedItem={selectedGenre}
                        onItemSelect={this.handleGenreSelect.bind(this)}
                    />
                </div>
                <div className="col">
                    {this.props.user && (
                        <Link to="/movies/new">
                            <button className="btn btn-primary">
                                Add Movie
                            </button>
                        </Link>
                    )}
                    <SearchBar
                        value={this.state.searchText}
                        onChange={this.handleSearch}
                        onClick={this.handleSearchClick}
                    />
                    <p>Showing {moviesCount} movies in the database </p>
                    <MoviesTable
                        movies={movies}
                        sortColumn={sortColumn}
                        onLike={this.handleToogleLike.bind(this)}
                        onDelete={this.handleDelete.bind(this)}
                        onSort={this.handleSort.bind(this)}
                        user={this.props.user}
                    />
                    <Pagination
                        currentPage={currentPage}
                        pageLength={pageLength}
                        totalItems={moviesCount}
                        onPageClick={this.handlePageClick.bind(this)}
                    />
                </div>
            </div>
        );
    }

    async doGetDataFromServer() {
        let { data } = await genreService.getGenres();
        const genres = [{ _id: '', name: 'All Genres' }, ...data];

        const { data: movies } = await movieService.getMovies();

        this.setState({
            ...this.state,
            movies,
            genres,
        });
    }

    handleSearchClick = (e) => {
        this.setState({
            selectedGenre: '',
        });
    };

    handleSearch = (e) => {
        this.setState({
            ...this.state,
            searchText: e.target.value,
        });
    };

    prepareData() {
        const {
            selectedGenre,
            movies: allMovies,
            sortColumn,
            currentPage,
            pageLength,
        } = this.state;

        const filtered =
            selectedGenre && selectedGenre._id
                ? allMovies.filter(
                      (movie) => movie.genre.name === selectedGenre.name
                  )
                : allMovies;

        const moviesCount = filtered.length;

        const sorted = _.orderBy(
            filtered,
            [sortColumn.path],
            [sortColumn.order]
        );

        const movies = paginate(sorted, currentPage, pageLength);

        return { movies, moviesCount };
    }

    handleSort(sortColumn) {
        this.setState({
            ...this.state,
            sortColumn,
        });
    }

    handleGenreSelect(genre) {
        this.setState({
            ...this.state,
            selectedGenre: genre,
            currentPage: 1,
            searchText: '',
        });
    }

    async handleDelete(movie) {
        const originalMovies = [...this.state.movies];
        const movies = [...originalMovies];

        const index = movies.indexOf(movie);
        movies.splice(index, 1);
        this.setState({ movies });

        try {
            await movieService.deleteMovie(movie._id);
        } catch (error) {
            if (error.response && error.response.status === 404)
                toast.error('movie was alreade deleted');
            this.setState({ movies: originalMovies });
        }
    }

    handleToogleLike(movie) {
        const movies = [...this.state.movies];
        const index = movies.indexOf(movie);
        movies[index] = { ...movie };
        movies[index].liked = !movies[index].liked;

        this.setState({
            movies,
        });
    }

    handlePageClick(page) {
        this.setState({
            ...this.state,
            currentPage: page,
        });
    }
}

export default Movies;
