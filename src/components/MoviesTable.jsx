import React from 'react';
import Like from './common/Like';
import Table from './common/Table';
import { Link } from 'react-router-dom';

const MoviesTable = ({
    movies,
    onLike,
    onDelete,
    onSort,
    sortColumn,
    user,
}) => {
    const columns = [
        {
            content: (movie) => (
                <Link to={'/movies/' + movie._id}>{movie['title']}</Link>
            ),

            path: 'title',
            label: 'Title',
        },
        { label: 'Genre', path: 'genre.name' },
        { label: 'Stock', path: 'numberInStock' },
        { label: 'Rate', path: 'dailyRentalRate' },
        {
            key: 'like',
            content: (movie) => <Like movie={movie} onToogleLike={onLike} />,
        },
        {
            key: 'delete',
            content: (movie) =>
                user && user.isAdmin ? (
                    <button
                        className="btn btn-danger"
                        onClick={() => onDelete(movie)}
                    >
                        Delete
                    </button>
                ) : null,
        },
    ];

    return (
        <Table
            columns={columns}
            data={movies}
            onSort={onSort}
            sortColumn={sortColumn}
        />
    );
};

export default MoviesTable;
