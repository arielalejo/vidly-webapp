import React, { Component } from 'react';

class Aux extends Component {
    state = { name: '' };

    handle = (e) => {
        const newname = e.target.value;
        this.setState({ name: newname });
        console.log(newname);
    };

    render() {
        return (
            /* value is the initial value ( when the element is rendered) */
            <input type="text" value={this.state.name} onChange={this.handle} />
        );
    }
}

export default Aux;
