import React from 'react';
import { NavLink } from 'react-router-dom';

const MoviesNavBar = (props) => {
    const { user } = props;

    return (
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <NavLink className="navbar-brand" to="/">
                Vidly
            </NavLink>
            <button
                className="navbar-toggler"
                type="button"
                data-toggle="collapse"
                data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent"
                aria-expanded="false"
                aria-label="Toggle navigation"
            >
                <span className="navbar-toggler-icon"></span>
            </button>
            <div
                className="collapse navbar-collapse"
                id="navbarSupportedContent"
            >
                <NavLink className="nav-link nav-item" to="/movies">
                    Movies
                </NavLink>
                <NavLink className="nav-link nav-item" to="/customers">
                    Customers
                </NavLink>
                <NavLink className="nav-link nav-item" to="/rentals">
                    Rentals
                </NavLink>
                {!user && (
                    <React.Fragment>
                        <NavLink className="nav-link nav-item" to="/register">
                            Register
                        </NavLink>
                        <NavLink className="nav-link nav-item" to="/login">
                            Login
                        </NavLink>
                    </React.Fragment>
                )}
                {user && (
                    <React.Fragment>
                        <NavLink className="nav-link nav-item" to="/profile">
                            {user.name}
                        </NavLink>
                        <NavLink className="nav-link nav-item" to="/logout">
                            Logout
                        </NavLink>
                    </React.Fragment>
                )}
            </div>
        </nav>
    );
};

export default MoviesNavBar;
