# vidly-webapp

**For running the app**

`yarn start`

and then open the url: [localhost:3000](localhost:3000) in the browser

![vidlyweb](/uploads/762de4131b48e912761741a3caa9e23a/vidlyweb.png)
