import React, { Component } from 'react';
import _ from 'lodash';
import { Link } from 'react-router-dom';

const arr = [
    {
        _id: '5b21ca3eeb7f6fbccd471815',
        title: 'Terminator',
        genre: { _id: '5b21ca3eeb7f6fbccd471818', name: 'Action' },
        numberInStock: 6,
        dailyRentalRate: 2.5,
        publishDate: '2018-01-03T19:04:28.809Z',
        liked: true,
    },
    {
        _id: '5b21ca3eeb7f6fbccd471816',
        title: 'Die Hard',
        genre: { _id: '5b21ca3eeb7f6fbccd471818', name: 'Action' },
        numberInStock: 5,
        dailyRentalRate: 2.5,
    },
    {
        _id: '5b21ca3eeb7f6fbccd471817',
        title: 'Get Out',
        genre: { _id: '5b21ca3eeb7f6fbccd471820', name: 'Thriller' },
        numberInStock: 8,
        dailyRentalRate: 3.5,
    },
    {
        _id: '5b21ca3eeb7f6fbccd471819',
        title: 'Trip to Italy',
        genre: { _id: '5b21ca3eeb7f6fbccd471814', name: 'Comedy' },
        numberInStock: 7,
        dailyRentalRate: 3.5,
    },
    {
        _id: '5b21ca3eeb7f6fbccd47181a',
        title: 'Airplane',
        genre: { _id: '5b21ca3eeb7f6fbccd471814', name: 'Comedy' },
        numberInStock: 7,
        dailyRentalRate: 3.5,
    },
];

const Like = ({ movie, onToogleLike }) => {
    let classes = 'fa fa-heart';
    if (!movie.liked) classes += '-o';
    return (
        <i
            style={{ cursor: 'pointer' }}
            className={classes}
            onClick={() => onToogleLike(movie)}
            aria-hidden="true"
        ></i>
    );
};

/* ************************************************************* */
/* sample table1 */
/* ************************************************************* */
class MyTabl1 extends Component {
    state = {
        movies: arr,
    };

    render() {
        return (
            <table className="table">
                <thead>
                    <tr>
                        <th>Title</th>
                        <th>Genre</th>
                        <th>Stock</th>
                        <th>Rate</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {this.state.movies.map((item) => (
                        <tr key={item._id}>
                            <td>{item.title}</td>
                            <td>{item.genre.name}</td>
                            <td>{item.numberInStock}</td>
                            <td>{item.dailyRentalRate}</td>
                            <td>
                                <Like
                                    movie={item}
                                    onToogleLike={this.handleToogleLike}
                                />
                            </td>
                        </tr>
                    ))}
                </tbody>
            </table>
        );
    }

    handleToogleLike = (movie) => {
        const movies = [...this.state.movies];
        const index = movies.indexOf(movie);
        movies[index] = { ...movie };
        movies[index].liked = !movies[index].liked;

        this.setState({
            movies,
        });
    };
}

/* ************************************************************* */
/* sample table2 */
/* ************************************************************* */
class MyTabl2 extends Component {
    state = { movies: arr };
    columns = [
        {
            headerlabel: 'TiTle',
            content: (movie) => <Link to="/">{movie.title}</Link>,
            moviepath: 'title',
        },
        { headerlabel: 'GenRe', moviepath: 'genre.name' },
        { headerlabel: 'StocK', moviepath: 'numberInStock' },
        { headerlabel: 'RatE', moviepath: 'dailyRentalRate' },
        {
            content: (movie) => (
                <Like movie={movie} onToogleLike={this.handleToogleLike} />
            ),
            key: 'lik',
        },
    ];
    render() {
        return (
            <table className="table">
                <thead>
                    <tr>
                        {this.columns.map((column) => (
                            <th key={column.moviepath}>{column.headerlabel}</th>
                        ))}
                    </tr>
                </thead>
                <tbody>
                    {this.state.movies.map((movie) => (
                        <tr>
                            {this.columns.map((column) => (
                                <td>{this.displayCell(movie, column)}</td>
                            ))}
                        </tr>
                    ))}
                </tbody>
            </table>
        );
    }

    displayCell = (movie, column) => {
        if (column.content) return column.content(movie);
        else return _.get(movie, column.moviepath);
    };

    handleToogleLike = (movie) => {
        const movies = [...this.state.movies];
        const index = movies.indexOf(movie);
        movies[index] = { ...movie };
        movies[index].liked = !movies[index].liked;

        this.setState({
            movies,
        });
    };
}

// export default MyTabl1;
export default MyTabl2;
