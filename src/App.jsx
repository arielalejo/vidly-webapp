import React, { Fragment, useState, useEffect } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import { ToastContainer } from 'react-toastify';
import Customers from './components/Customers';
import Movies from './components/Movies';
import MoviesNavBar from './components/MoviesNavBar';
import NotFound from './components/NotFound';
import Rentals from './components/Rentals';
import MovieForm from './components/MovieForm';
import LoginForm from './components/LoginForm';
import Logout from './components/Logout';
import RegisterForm from './components/RegisterForm';
import authService from './services/authService';
import 'react-toastify/dist/ReactToastify.css';
import ProtectedRoute from './components/common/ProtectedRoute';

const App = () => {
    /* should be an empty object, but and empty object allow conditional rendering */
    const [user, setUser] = useState('');

    useEffect(() => {
        const user = authService.getLoggedUser();
        setUser(user);
    }, []);

    return (
        <Fragment>
            <ToastContainer />
            <MoviesNavBar user={user} />
            <div className="content mt-4">
                <Switch>
                    <Route path="/login" component={LoginForm} />
                    <Route path="/register" component={RegisterForm} />
                    <Route path="/logout" component={Logout} />
                    <ProtectedRoute path="/movies/:id" component={MovieForm} />
                    <Route
                        path="/movies"
                        render={(props) => <Movies {...props} user={user} />}
                    />
                    <ProtectedRoute
                        path="/rentals"
                        forRender={(props) => <Rentals {...props} value={123} />}
                    />
                    <Route path="/customers" component={Customers} />
                    <Route path="/not-found" component={NotFound} />
                    <Redirect from="/" exact to="/movies" />
                    <Redirect to="/not-found" />
                </Switch>
            </div>
        </Fragment>
    );
};

export default App;
