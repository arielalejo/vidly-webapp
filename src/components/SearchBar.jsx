import React, { Component } from 'react';

class SearchBar extends Component {
    render() {
        //const {value, onChange} =
        return (
            <div>
                <input
                    type="text"
                    name="searchbar"
                    id="searchbar"
                    className="form-control mt-4 mb-4"
                    {...this.props}
                />
            </div>
        );
    }
}

export default SearchBar;
