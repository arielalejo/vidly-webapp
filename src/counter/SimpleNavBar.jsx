import React from 'react';

const SimpleNavBar = ({ totalCounters }) => {
    return (
        <nav className="navbar navbar-light bg-light">
            <span className="navbar-brand ">
                Total{' '}
                <span className="badge badge-pill badge-dark">
                    {totalCounters}
                </span>
            </span>
        </nav>
    );
};

export default SimpleNavBar;
