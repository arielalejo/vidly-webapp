import * as Sentry from '@sentry/react';
import { Integrations } from '@sentry/tracing';

export const init = () => {
    Sentry.init({
        dsn:
            'https://fed53e11f69444cb911a6bea86615f3d@o456421.ingest.sentry.io/5456353',
        integrations: [new Integrations.BrowserTracing()],

        // We recommend adjusting this value in production, or using tracesSampler
        // for finer control
        tracesSampleRate: 1.0,
    });
};

export const log = (error) => {
    Sentry.captureException(error);
};
