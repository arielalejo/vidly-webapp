import React from 'react';

const GroupList = ({
    items,
    valueProperty,
    textProperty,
    selectedItem,
    onItemSelect,
}) => {
    return (
        <ul className="list-group">
            {items.map((item) => (
                <li
                    key={item[valueProperty]}
                    className={
                        item === selectedItem
                            ? 'clickable list-group-item active'
                            : 'clickable list-group-item'
                    }
                    onClick={() => onItemSelect(item)}
                >
                    {item[textProperty]}
                </li>
            ))}
        </ul>
    );
};

GroupList.defaultProps = {
    valueProperty: '_id',
    textProperty: 'name',
};

export default GroupList;
