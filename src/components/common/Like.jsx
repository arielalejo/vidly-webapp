import React from 'react';

const Like = ({ movie, onToogleLike }) => {
    let classes = 'fa fa-heart';
    if (!movie.liked) classes += '-o';
    return (
        <i
            style={{ cursor: 'pointer' }}
            className={classes}
            onClick={() => onToogleLike(movie)}
            aria-hidden="true"
        ></i>
    );
};

export default Like;
